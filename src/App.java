import com.devcamp.j40_javabasic.s70.Duck;
import com.devcamp.j40_javabasic.s70.Fish;
import com.devcamp.j40_javabasic.s70.Zebra;

public class App {
    public static void main(String[] args) throws Exception {

        Duck duck = new Duck();
        Fish fish = new Fish();
        Zebra zebra = new Zebra();
        System.out.println(duck);
        System.out.println(fish);
        System.out.println(duck);
    }
}
