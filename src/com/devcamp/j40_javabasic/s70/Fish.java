package com.devcamp.j40_javabasic.s70;

public class Fish extends Aminal implements ISwimable {

 public void animalSound() {
  System.out.println(".....");
 }

 @Override
 public void swim() {
  System.out.println("Fish swimming");
 }
 

}
