package com.devcamp.j40_javabasic.s70;

public class Aminal {
protected String name;
protected int age;
protected float weight;


 @Override
 public String toString() {
  return "[name=" + name + ", age=" + age + ", weight=" + weight + "]";
}

 public String getName() {
  return name;
}
public static void main(String[] args) {
 Duck duck = new Duck();
 duck.name = "Duck";
 duck.age = 3;
 duck.weight= 12.00f;
 duck.animalSound();
 duck.walk();
 Fish fish = new Fish();
 fish.name = "Fish";
 fish.age = 2;
 fish.weight= 2.00f;
 fish.animalSound();
 fish.swim();
 Zebra zebra = new Zebra();
 zebra.name = "Zebra";
 zebra.age = 15;
 zebra.weight= 60.00f;
 zebra.animalSound();
 zebra.running();
 System.out.println(duck);
 System.out.println(fish);
 System.out.println(zebra);
}
        
}
