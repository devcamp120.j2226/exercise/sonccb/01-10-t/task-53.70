package com.devcamp.j40_javabasic.s70;

public class Duck  extends Aminal implements IWalkable {
 
 public void animalSound() {
  System.out.println("Quack quack");
  
 }

 @Override
 public void walk() {
  System.out.println("Duck walking...");
 }
}

